package service.loan;

import service.loan.model.Lender;
import service.loan.model.LenderPool;
import service.loan.model.Quote;

import java.math.BigDecimal;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Optional;

public class LoanCalculator {

    private static final BigDecimal DEFAULT_LOAN_CHUNK = BigDecimal.TEN;

    Optional<Quote> getQuote(LenderPool lenderPool, BigDecimal requestedLoanAmount) {
        if (Objects.nonNull(lenderPool) && (lenderPool.hasAvailableFundsForLoan(requestedLoanAmount))) {
            Quote quote = new Quote(requestedLoanAmount);

            LinkedList<Lender> lenderList = (LinkedList<Lender>) lenderPool.getPrioritisedLenderList();
            Lender currentLender = lenderList.remove();

            while (Objects.nonNull(currentLender)) {
                if (currentLender.hasAvailableAmountToLoan(quote.getLenderLoanAmount(currentLender), DEFAULT_LOAN_CHUNK)) {
                    quote.addLoan(currentLender, DEFAULT_LOAN_CHUNK);
                }
                if (quote.hasReachedRequestedLoanAmount()) {
                    return Optional.of(quote);
                }
                if (currentLender.hasAvailableAmountToLoan(quote.getLenderLoanAmount(currentLender), DEFAULT_LOAN_CHUNK)) {
                    lenderList.add(currentLender);
                }
                currentLender = lenderList.remove();
            }
        }
        return Optional.empty();
    }
}

// lenderPool = new LinkedList<>(sort(lenderPool));
//
// currentLender = lenderPool.remove();
// while (currentLender isNotNull) {
//   if (currentLender.availableAmount >= 10) {
//     currentLender.availableAmount -= 10;
//     currentLoan.lenders.get(currentLender).loanAmount += 10;
//   }
//
//   if(currentLoan >= requestedLoan) {
//     return quote;
//   } else if (currentLender.availableAmount >= 10) {
//     lenderPool.push(currentLender)
//   }
// }
