package service.loan;

import service.loan.io.LoanQuoteCli;
import service.loan.io.MarketDataBuilder;
import service.loan.model.LenderPool;
import service.loan.model.Quote;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

public class LoanQuoteService {

    private LoanQuoteCli loanQuoteCli;
    private LoanCalculator loanCalculator;
    private MarketDataBuilder marketDataBuilder;
    private LenderPool lenderPool;

    public LoanQuoteService(LoanQuoteCli loanQuoteCli, LoanCalculator loanCalculator, MarketDataBuilder marketDataBuilder, LenderPool lenderPool) {
        this.loanQuoteCli = Objects.requireNonNull(loanQuoteCli);
        this.loanCalculator = Objects.requireNonNull(loanCalculator);
        this.marketDataBuilder = Objects.requireNonNull(marketDataBuilder);
        this.lenderPool = Objects.requireNonNull(lenderPool);
    }

    public void run(String[] args) {
        try {
            String[] loanQuoteArgs = loanQuoteCli.parseCliArgs(args);

            String marketDataFilename = loanQuoteArgs[0];
            lenderPool.setLenders(marketDataBuilder.buildMarketData(marketDataFilename));
            BigDecimal requestedLoanAmount = new BigDecimal(loanQuoteArgs[1]);
            if (isValidLoanRequest(requestedLoanAmount)) {
                Optional<Quote> quote = loanCalculator.getQuote(lenderPool, requestedLoanAmount);
                if (quote.isPresent()) {
                    loanQuoteCli.printLine(quote.get().toString());
                } else {
                    loanQuoteCli.printLine("We are unable to provide a quote at this time. Please try again later.");
                }
            } else {
                loanQuoteCli.printLine("Please enter a valid loan amount. Loans can be requested in £100 increments, from £1000 up to £15000 inclusive.");
            }
        } catch (IllegalArgumentException | FileNotFoundException e) {
            loanQuoteCli.printLine(e.getMessage());
        }
    }

    public boolean isValidLoanRequest(BigDecimal requestedLoanAmount) {
        return isWithinRequestLimits(requestedLoanAmount) && isValidLoanDenomination(requestedLoanAmount);
    }

    public boolean isWithinRequestLimits(BigDecimal requestedLoanAmount) {
        return (requestedLoanAmount.compareTo(BigDecimal.valueOf(1000)) >= 0) &&
                (requestedLoanAmount.compareTo(BigDecimal.valueOf(1500)) <= 0);
    }

    public boolean isValidLoanDenomination(BigDecimal requestedLoanAmount) {
        return requestedLoanAmount.remainder(BigDecimal.valueOf(100)).compareTo(BigDecimal.ZERO) == 0;
    }
}
