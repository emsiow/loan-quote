package service.loan;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class InterestCalculator {

    public static BigDecimal monthlyCompoundingInterest(BigDecimal principalAmount, BigDecimal interestRate, int periods) {
        if (interestRate.equals(BigDecimal.ZERO)) {
            return principalAmount.divide(BigDecimal.valueOf(periods), 2, RoundingMode.FLOOR);
        }

        BigDecimal monthlyInterestRate = interestRate.divide(BigDecimal.valueOf(12), 100, RoundingMode.FLOOR);
        BigDecimal numerator = monthlyInterestRate
                .multiply(principalAmount
                        .multiply(BigDecimal.ONE
                                .add(monthlyInterestRate).pow(periods)));
        BigDecimal denominator = BigDecimal.ONE.add(monthlyInterestRate).pow(periods).subtract(BigDecimal.ONE);

        return numerator.divide(denominator, 2, RoundingMode.FLOOR);
    }

    static double monthlyCompoundingInterest(double principalAmount, double interestRate, int periods) {
        if (interestRate == 0) {
            return principalAmount / periods;
        }

        double monthlyInterestRate = interestRate / 12;
        double numerator = monthlyInterestRate * principalAmount * Math.pow(1 + monthlyInterestRate, periods);
        double denominator = Math.pow(1 + monthlyInterestRate, periods) - 1;

        return numerator / denominator;
    }

}
