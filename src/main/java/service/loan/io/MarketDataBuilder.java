package service.loan.io;

import com.opencsv.bean.CsvToBeanBuilder;
import service.loan.model.Lender;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

public class MarketDataBuilder {

    public List<Lender> buildMarketData(String filepath) throws FileNotFoundException {
        List<Lender> lenders;
        try {
            lenders = new CsvToBeanBuilder<Lender>(new FileReader(filepath))
                    .withType(Lender.class)
                    .build()
                    .parse();
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException("Could not read from file: " + filepath + "\nPlease check that specified market data file is correct and try again.");
        }
        return lenders;
    }
}
