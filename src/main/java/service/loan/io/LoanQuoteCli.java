package service.loan.io;

public class LoanQuoteCli {

    public void printLine(String text) {
        System.out.println(text);
    }

    public String[] parseCliArgs(String[] args) throws IllegalArgumentException {
        if (args.length < 1) {
            throw new IllegalArgumentException("Market data file was not specified.\nUsage: > [application] [market_file] [loan_amount]");
        } else if (args.length < 2) {
            throw new IllegalArgumentException("Loan amount was not specified.\nUsage: > [application] [market_file] [loan_amount]");
        }
        return new String[]{args[0],args[1]};
    }
}
