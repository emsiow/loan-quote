package service.loan.model;


import com.opencsv.bean.CsvBindByName;

import java.math.BigDecimal;

public class Lender implements Comparable<Lender> {
    @CsvBindByName(column = "Lender", required = true)
    private String name;

    @CsvBindByName(column = "Rate", required = true)
    private BigDecimal interestRate;

    @CsvBindByName(column = "Available", required = true)
    private BigDecimal availableAmount;

    public Lender() {
    }

    public Lender(String name, BigDecimal interestRate, BigDecimal availableAmount) {
        this.name = name;
        this.interestRate = interestRate;
        this.availableAmount = availableAmount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(BigDecimal interestRate) {
        this.interestRate = interestRate;
    }

    public BigDecimal getAvailableAmount() {
        return availableAmount;
    }

    public void setAvailableAmount(BigDecimal availableAmount) {
        this.availableAmount = availableAmount;
    }

    public boolean hasAvailableAmountToLoan(BigDecimal currentLoanAmount, BigDecimal loanChunkAmount) {
        return this.availableAmount.compareTo(currentLoanAmount.add(loanChunkAmount)) >= 0;
    }

    @Override
    public String toString() {
        return String.join(", ", this.name, this.interestRate.toString(), this.availableAmount.toString());
    }

    @Override
    public int compareTo(Lender otherLender) {
        return this.interestRate.compareTo(otherLender.interestRate);
    }
}
