package service.loan.model;

import java.math.BigDecimal;

public class Loan {
    private Lender lender;
    private BigDecimal loanAmount;
    private BigDecimal rate;

    public Lender getLender() {
        return lender;
    }

    public BigDecimal getRate() {
        return rate;
    }
}
