package service.loan.model;

import service.loan.InterestCalculator;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.*;

public class Quote {

    private static final int DEFAULT_REPAYMENT_MONTHS = 36;
    private static DecimalFormat rateFormatter;
    private static DecimalFormat repaymentFormatter;

    private BigDecimal requestedLoanAmount;
    private Map<Lender, BigDecimal> loans;

    public Quote(BigDecimal requestedLoanAmount) {
        this.requestedLoanAmount = requestedLoanAmount;
        this.loans = new HashMap<>();
        rateFormatter = new DecimalFormat();
        rateFormatter.setMaximumFractionDigits(1);
        rateFormatter.setMinimumFractionDigits(1);

        repaymentFormatter = new DecimalFormat();
        repaymentFormatter.setMaximumFractionDigits(2);
        repaymentFormatter.setMinimumFractionDigits(2);
        repaymentFormatter.setGroupingUsed(false);
    }

    public BigDecimal getRequestedLoanAmount() {
        return requestedLoanAmount;
    }

    public void addLoan(Lender lender, BigDecimal loanAmount) {
        BigDecimal existingLoan = loans.get(lender);
        if (Objects.nonNull(existingLoan)) {
            loans.put(lender, existingLoan.add(loanAmount));
        } else {
            loans.put(lender, loanAmount);
        }
    }

    public BigDecimal getLenderLoanAmount(Lender lender) {
        BigDecimal loanAmount = this.loans.get(lender);
        if (Objects.isNull(loanAmount)) {
            loanAmount = BigDecimal.ZERO;
        }
        return loanAmount;
    }

    public BigDecimal getTotalLoanAmount() {
        return this.loans.values().stream()
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public Map<Lender, BigDecimal> getLoans() {
        return loans;
    }

    public boolean hasReachedRequestedLoanAmount() {
        return this.getTotalLoanAmount().compareTo(this.requestedLoanAmount) >= 0;
    }

    public BigDecimal getInterestRate() {
        return loans.keySet().stream()
                .map(lender -> lender.getInterestRate()
                        .multiply(loans.get(lender))
                        .divide(requestedLoanAmount, RoundingMode.FLOOR))
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    BigDecimal getMonthlyRepayment() {
        return InterestCalculator.monthlyCompoundingInterest(requestedLoanAmount, getInterestRate(), DEFAULT_REPAYMENT_MONTHS);
    }

    BigDecimal getTotalRepaymentAmount() {
        return this.getMonthlyRepayment().multiply(BigDecimal.valueOf(DEFAULT_REPAYMENT_MONTHS));
    }

    @Override
    public String toString() {
        return "Requested amount: £" + this.requestedLoanAmount + "\n" +
                "Rate: " + rateFormatter.format(this.getInterestRate().multiply(BigDecimal.valueOf(100).setScale(1, RoundingMode.FLOOR))) + "%\n" +
                "Monthly repayment: £" + repaymentFormatter.format(this.getMonthlyRepayment().setScale(2, RoundingMode.FLOOR)) + "\n" +
                "Total repayment: £" + repaymentFormatter.format(this.getTotalRepaymentAmount().setScale(2, RoundingMode.FLOOR)) + "\n";
    }
}
