package service.loan.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class LenderPool {

    private List<Lender> lenders;

    public LenderPool() {
        this.lenders = new ArrayList<>();
    }

    public List<Lender> getLenders() {
        return lenders;
    }

    public void setLenders(List<Lender> lenders) {
        this.lenders = lenders;
    }

    BigDecimal totalAvailableAmount() {
        return this.lenders.stream()
                .map(Lender::getAvailableAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public boolean hasAvailableFundsForLoan(BigDecimal requestedLoan) {
        return this.totalAvailableAmount().compareTo(requestedLoan) >= 0;
    }

    public List<Lender> getPrioritisedLenderList() {
        List<Lender> lenders = Optional.ofNullable(this.lenders)
                .map(List::stream)
                .orElseGet(Stream::empty)
                .collect(Collectors.toCollection(LinkedList::new));
        lenders.sort(Lender::compareTo);
        return lenders;
    }
}
