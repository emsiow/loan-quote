package service;

import service.loan.LoanCalculator;
import service.loan.LoanQuoteService;
import service.loan.io.MarketDataBuilder;
import service.loan.io.LoanQuoteCli;
import service.loan.model.LenderPool;

public class Application {

    private static final LoanQuoteService loanQuoteService = new LoanQuoteService(
            new LoanQuoteCli(), new LoanCalculator(), new MarketDataBuilder(), new LenderPool());

    public static void main(String[] args) {
        loanQuoteService.run(args);
    }
}
