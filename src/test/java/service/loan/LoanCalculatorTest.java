package service.loan;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import service.loan.model.Lender;
import service.loan.model.LenderPool;
import service.loan.model.Quote;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;

@RunWith(MockitoJUnitRunner.class)
public class LoanCalculatorTest {

    private LoanCalculator loanCalculator = new LoanCalculator();

    @Mock
    private LenderPool lenderPool;

    @Test
    public void shouldReturnEmptyWhenThereAreNoLenders() throws Exception {
        Optional<Quote> quoteOptional = loanCalculator.getQuote(lenderPool, BigDecimal.valueOf(1500));
        assertFalse(quoteOptional.isPresent());
    }

    @Test
    public void shouldReturnEmptyWhenLenderPoolDoesNotHaveAvailableFundsForLoan() throws Exception {
        doReturn(false).when(lenderPool).hasAvailableFundsForLoan(any());

        Optional<Quote> quoteOptional = loanCalculator.getQuote(lenderPool, BigDecimal.valueOf(1500));
        assertFalse(quoteOptional.isPresent());
    }

    @Test
    public void shouldReturnEmptyWhenLenderPoolDoesNotHaveEnoughAvailable() throws Exception {
        List<Lender> lenders = new ArrayList<>();
        Lender lender1 = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(100));
        Lender lender2 = new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(100));
        lenders.add(lender1);
        lenders.add(lender2);
        LenderPool lenderPool = new LenderPool();
        lenderPool.setLenders(lenders);

        Optional<Quote> quoteOptional = loanCalculator.getQuote(lenderPool, BigDecimal.valueOf(210));

        assertFalse(quoteOptional.isPresent());
    }

    @Test
    public void shouldReturnEmptyIfThereAreNoLenders() throws Exception {
        Optional<Quote> quote = loanCalculator.getQuote(new LenderPool(), BigDecimal.valueOf(1500));
        assertEquals(Optional.empty(), quote);
    }

    @Test
    public void shouldReturnQuoteWithInterestRateFromSingleLender() throws Exception {
        List<Lender> lenderList = new LinkedList<>();
        lenderList.add(new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(800)));

        doReturn(true).when(lenderPool).hasAvailableFundsForLoan(any());
        doReturn(lenderList).when(lenderPool).getPrioritisedLenderList();

        Quote quote = loanCalculator.getQuote(lenderPool, BigDecimal.valueOf(300)).get();

        BigDecimal requestedLoanAmount = quote.getRequestedLoanAmount();
        assertEquals(BigDecimal.valueOf(300).toString(), requestedLoanAmount.toString());

        BigDecimal interestRate = quote.getInterestRate();
        assertEquals(BigDecimal.valueOf(0.065).toString(), interestRate.toString());
    }

    @Test
    public void shouldReturnQuoteUsingTenPoundLoanChunksFromMoreThanOneLender() throws Exception {
        List<Lender> lenders = new ArrayList<>();
        Lender lender1 = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(100));
        Lender lender2 = new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(100));
        lenders.add(lender1);
        lenders.add(lender2);
        LenderPool lenderPool = new LenderPool();
        lenderPool.setLenders(lenders);

        Quote quote = loanCalculator.getQuote(lenderPool, BigDecimal.valueOf(20)).get();

        Map<Lender, BigDecimal> loans = quote.getLoans();
        assertEquals(2, loans.size());
        assertEquals(BigDecimal.TEN, loans.get(lender1));
        assertEquals(BigDecimal.TEN, loans.get(lender2));
    }

    @Test
    public void shouldReturnQuoteWithMultipleChunksFromOneOfMultipleLenders() throws Exception {
        List<Lender> lenders = new ArrayList<>();
        Lender lender1 = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(100));
        Lender lender2 = new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(100));
        lenders.add(lender1);
        lenders.add(lender2);
        LenderPool lenderPool = new LenderPool();
        lenderPool.setLenders(lenders);

        Quote quote = loanCalculator.getQuote(lenderPool, BigDecimal.valueOf(30)).get();

        Map<Lender, BigDecimal> loans = quote.getLoans();
        assertEquals(2, loans.size());
        assertEquals(BigDecimal.valueOf(20), loans.get(lender1));
        assertEquals(BigDecimal.TEN, loans.get(lender2));
    }
}