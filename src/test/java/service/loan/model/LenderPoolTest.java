package service.loan.model;

import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class LenderPoolTest {

    private LenderPool lenderPool = new LenderPool();

    @Test
    public void shouldReturnZeroWhenLenderPoolIsEmpty() throws Exception {
        BigDecimal totalAvailableAmount = lenderPool.totalAvailableAmount();
        assertEquals(BigDecimal.ZERO, totalAvailableAmount);
    }

    @Test
    public void shouldReturnSumOfAvailableAmountsInLenderPool() throws Exception {
        List<Lender> lenders = new ArrayList<>();
        lenders.add(new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(800)));
        lenders.add(new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(5)));
        lenderPool.setLenders(lenders);

        BigDecimal totalAvailableAmount = lenderPool.totalAvailableAmount();
        assertEquals(BigDecimal.valueOf(805), totalAvailableAmount);
    }

    @Test
    public void shouldReturnFalseWhenLenderPoolTotalIsLessThanRequestedLoan() throws Exception {
        List<Lender> lenders = new ArrayList<>();
        lenders.add(new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(800)));
        lenders.add(new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(5)));
        lenderPool.setLenders(lenders);

        boolean hasAvailableFundsForLoan = lenderPool.hasAvailableFundsForLoan(BigDecimal.valueOf(1500));
        assertFalse(hasAvailableFundsForLoan);
    }

    @Test
    public void shouldReturnTrueWhenLenderPoolTotalIsEqualToRequestedLoan() throws Exception {
        List<Lender> lenders = new ArrayList<>();
        lenders.add(new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(1000)));
        lenders.add(new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(500)));
        lenderPool.setLenders(lenders);

        boolean hasAvailableFundsForLoan = lenderPool.hasAvailableFundsForLoan(BigDecimal.valueOf(1500));
        assertTrue(hasAvailableFundsForLoan);
    }

    @Test
    public void shouldReturnTrueWhenLenderPoolTotalIsGreaterThanRequestedLoan() throws Exception {
        List<Lender> lenders = new ArrayList<>();
        lenders.add(new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(1000)));
        lenders.add(new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(500)));
        lenders.add(new Lender("Mary", BigDecimal.valueOf(0.08), BigDecimal.valueOf(600)));
        lenderPool.setLenders(lenders);

        boolean hasAvailableFundsForLoan = lenderPool.hasAvailableFundsForLoan(BigDecimal.valueOf(1500));
        assertTrue(hasAvailableFundsForLoan);
    }

    @Test
    public void shouldReturnLinkedListOfLendersSortedByRate() throws Exception {
        Lender lender1 = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(0));
        Lender lender2 = new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(0));
        Lender lender3 = new Lender("Mary", BigDecimal.valueOf(0.03), BigDecimal.valueOf(0));
        List<Lender> lenders = new ArrayList<>(Arrays.asList(lender1, lender2, lender3));
        lenderPool.setLenders(lenders);

        List<Lender> expectedLenderList = new LinkedList<>(Arrays.asList(lender3, lender1, lender2));
        List<Lender> lenderList = lenderPool.getPrioritisedLenderList();
        assertEquals(expectedLenderList, lenderList);
        assertTrue(lenderList instanceof LinkedList);
    }
}