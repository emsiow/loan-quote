package service.loan.model;

import org.junit.Test;

import java.math.BigDecimal;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

public class QuoteTest {

    @Test
    public void shouldFormatQuoteForCli() throws Exception {
        Path dataDir = Paths.get("src", "test", "data");
        String expectedQuoteString = new String(Files.readAllBytes(Paths.get(dataDir + "/expectedQuote")))
                .replace("\n", "");

        Quote quote = new Quote(BigDecimal.valueOf(1000));
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.070), BigDecimal.valueOf(1500));
        quote.addLoan(lender, BigDecimal.valueOf(1000));

        String quoteString = quote.toString()
                .replace("\n", "");
        assertEquals(expectedQuoteString, quoteString);
    }

    @Test
    public void shouldAddNewLoanMapping() throws Exception {
        Quote quote = new Quote(BigDecimal.valueOf(300));
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(500));
        quote.addLoan(lender, BigDecimal.valueOf(300));
        BigDecimal loanAmount = quote.getLoans().get(lender);
        assertEquals(BigDecimal.valueOf(300), loanAmount);
    }

    @Test
    public void shouldAddLoanAmountToExistingLoanMapping() throws Exception {
        Quote quote = new Quote(BigDecimal.valueOf(300));
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(1500));
        quote.addLoan(lender, BigDecimal.valueOf(300));
        quote.addLoan(lender, BigDecimal.valueOf(300));
        BigDecimal loanAmount = quote.getLoans().get(lender);
        assertEquals(BigDecimal.valueOf(600), loanAmount);
    }

    @Test
    public void shouldReturnInterestRateForSingleLender() throws Exception {
        Quote quote = new Quote(BigDecimal.valueOf(300));
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(1500));
        quote.addLoan(lender, BigDecimal.valueOf(300));
        BigDecimal interestRate = quote.getInterestRate();
        assertEquals(BigDecimal.valueOf(0.065), interestRate);
    }

    @Test
    public void shouldReturnBlendedInterestRateForDifferentLenderRates() throws Exception {
        Quote quote = new Quote(BigDecimal.valueOf(800));
        Lender lender1 = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(300));
        Lender lender2 = new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(500));
        quote.addLoan(lender1, BigDecimal.valueOf(300));
        quote.addLoan(lender2, BigDecimal.valueOf(500));
        BigDecimal interestRate = quote.getInterestRate();
        assertEquals(BigDecimal.valueOf(0.074), interestRate);
    }

    @Test
    public void shouldReturnZeroCurrentLoanAmountWhenNoLoans() throws Exception {
        Quote quote = new Quote(BigDecimal.valueOf(800));
        BigDecimal currentLoanAmount = quote.getTotalLoanAmount();
        assertEquals(BigDecimal.ZERO, currentLoanAmount);
    }

    @Test
    public void shouldReturnSumOfCurrentLoanAmounts() throws Exception {
        Quote quote = new Quote(BigDecimal.valueOf(800));
        Lender lender1 = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(300));
        Lender lender2 = new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(500));
        quote.addLoan(lender1, BigDecimal.valueOf(200));
        quote.addLoan(lender2, BigDecimal.valueOf(100));
        BigDecimal currentLoanAmount = quote.getTotalLoanAmount();
        assertEquals(BigDecimal.valueOf(300), currentLoanAmount);
    }

    @Test
    public void shouldReturnZeroLenderLoanAmountWhenLenderHasNotLoaned() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(20));
        Quote quote = new Quote(BigDecimal.valueOf(800));
        BigDecimal lenderLoanAmount = quote.getLenderLoanAmount(lender);
        assertEquals(BigDecimal.ZERO, lenderLoanAmount);
    }

    @Test
    public void shouldReturnLenderLoanAmountWhenLenderHasLoaned() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(1500));
        Quote quote = new Quote(BigDecimal.valueOf(800));
        quote.addLoan(lender, BigDecimal.valueOf(20));
        BigDecimal lenderLoanAmount = quote.getLenderLoanAmount(lender);
        assertEquals(BigDecimal.valueOf(20), lenderLoanAmount);
    }

    @Test
    public void shouldBeTrueWhenTotalLoanAmountIsGreaterThanRequestedLoanAmount() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(1500));
        Quote quote = new Quote(BigDecimal.valueOf(800));
        quote.addLoan(lender, BigDecimal.valueOf(1000));

        assertTrue(quote.hasReachedRequestedLoanAmount());
    }

    @Test
    public void shouldBeTrueWhenTotalLoanAmountIsEqualToRequestedLoanAmount() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(800));
        Quote quote = new Quote(BigDecimal.valueOf(800));
        quote.addLoan(lender, BigDecimal.valueOf(800));

        assertTrue(quote.hasReachedRequestedLoanAmount());
    }

    @Test
    public void shouldBeFalseWhenTotalLoanAmountIsLessThanRequestedLoanAmount() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(800));
        Quote quote = new Quote(BigDecimal.valueOf(800));
        quote.addLoan(lender, BigDecimal.valueOf(400));

        assertFalse(quote.hasReachedRequestedLoanAmount());
    }

    @Test
    public void shouldReturn36MonthlyCompoundInterestForMonthlyRepayment() throws Exception {
        Quote quote = new Quote(BigDecimal.valueOf(1000));
        quote.addLoan(new Lender("Janelle", BigDecimal.valueOf(0.07), BigDecimal.valueOf(1000)), BigDecimal.valueOf(1000));

        assertEquals(BigDecimal.valueOf(30.87), quote.getMonthlyRepayment());
    }

    @Test
    public void shouldCalculateTotalRepaymentAs36MonthlyRepayments() throws Exception {
        Quote quote = new Quote(BigDecimal.valueOf(1000));
        quote.addLoan(new Lender("Janelle", BigDecimal.valueOf(0.07), BigDecimal.valueOf(1000)), BigDecimal.valueOf(1000));

        assertEquals(BigDecimal.valueOf(1111.32), quote.getTotalRepaymentAmount());
    }
}