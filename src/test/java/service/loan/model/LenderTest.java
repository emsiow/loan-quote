package service.loan.model;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

public class LenderTest {

    @Test
    public void shouldBeNegativeOneWhenOtherLenderHasHigherInterestRate() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(0));
        Lender otherLender = new Lender("Cindi", BigDecimal.valueOf(0.08), BigDecimal.valueOf(0));
        int compareToValue = lender.compareTo(otherLender);
        assertEquals(-1, compareToValue);
    }

    @Test
    public void shouldBeZeroWhenOtherLenderHasSameInterestRate() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(0));
        Lender otherLender = new Lender("Cindi", BigDecimal.valueOf(0.065), BigDecimal.valueOf(0));
        int compareToValue = lender.compareTo(otherLender);
        assertEquals(0, compareToValue);
    }

    @Test
    public void shouldBeOneWhenOtherLenderHasLowerInterestRate() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.09), BigDecimal.valueOf(0));
        Lender otherLender = new Lender("Cindi", BigDecimal.valueOf(0.03), BigDecimal.valueOf(0));
        int compareToValue = lender.compareTo(otherLender);
        assertEquals(1, compareToValue);
    }

    @Test
    public void shouldBeTrueWhenCurrentLoanAmountAndLoanChunkIsLessThanAvailableAmount() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(20));
        BigDecimal currentLoanAmount = BigDecimal.valueOf(0);
        BigDecimal loanChunkAmount = BigDecimal.valueOf(10);
        boolean hasAvailableAmountToLoan = lender.hasAvailableAmountToLoan(currentLoanAmount, loanChunkAmount);
        assertTrue(hasAvailableAmountToLoan);
    }

    @Test
    public void shouldBeTrueWhenCurrentLoanAmountAndLoanChunkIsEqualToAvailableAmount() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(20));
        BigDecimal currentLoanAmount = BigDecimal.valueOf(10);
        BigDecimal loanChunkAmount = BigDecimal.valueOf(10);
        boolean hasAvailableAmountToLoan = lender.hasAvailableAmountToLoan(currentLoanAmount, loanChunkAmount);
        assertTrue(hasAvailableAmountToLoan);
    }

    @Test
    public void shouldBeFalseWhenCurrentLoanAmountAndLoanChunkIsGreaterThanAvailableAmount() throws Exception {
        Lender lender = new Lender("Janelle", BigDecimal.valueOf(0.065), BigDecimal.valueOf(10));
        BigDecimal currentLoanAmount = BigDecimal.valueOf(5);
        BigDecimal loanChunkAmount = BigDecimal.valueOf(10);
        boolean hasAvailableAmountToLoan = lender.hasAvailableAmountToLoan(currentLoanAmount, loanChunkAmount);
        assertFalse(hasAvailableAmountToLoan);
    }
}