package service.loan.io;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.assertArrayEquals;

public class LoanQuoteCliTest {

    private LoanQuoteCli loanQuoteCli = new LoanQuoteCli();

    @Rule
    public ExpectedException thrownException = ExpectedException.none();

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenMarketDataIsNotProvided() throws Exception {
        thrownException.expect(IllegalArgumentException.class);
        thrownException.expectMessage("Market data file was not specified.\nUsage: > [application] [market_file] [loan_amount]");
        String[] args = {};
        loanQuoteCli.parseCliArgs(args);
    }

    @Test
    public void shouldThrowIllegalArgumentExceptionWhenLoanAmountIsNotProvided() throws Exception {
        thrownException.expect(IllegalArgumentException.class);
        thrownException.expectMessage("Loan amount was not specified.\nUsage: > [application] [market_file] [loan_amount]");
        String[] args = {"marketDataFilename"};
        loanQuoteCli.parseCliArgs(args);
    }

    @Test
    public void shouldNotThrowExceptionWhenMarketDataAndLoanAmountAreSpecified() throws Exception {
        String[] args = {"marketDataFilename", "loanAmount"};
        loanQuoteCli.parseCliArgs(args);
    }

    @Test
    public void shouldParseMarketDataFilenameAndLoanAmountFromCommandLineArgs() throws Exception {
        String[] args = {"marketDataFilename", "loanAmount", "extraArgumentToIgnore"};
        String[] loanQuoteArgs = loanQuoteCli.parseCliArgs(args);
        assertArrayEquals(new String[]{"marketDataFilename", "loanAmount"}, loanQuoteArgs);
    }
}