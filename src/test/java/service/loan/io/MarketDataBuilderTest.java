package service.loan.io;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import service.loan.model.Lender;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class MarketDataBuilderTest {

    private MarketDataBuilder marketDataBuilder = new MarketDataBuilder();

    @Rule
    public ExpectedException thrownException = ExpectedException.none();

    @Test
    public void shouldThrowFileNotFoundExceptionWhenProvidedFileDoesNotExist() throws Exception {
        thrownException.expect(FileNotFoundException.class);
        String filepath = "fileDoesNotExist";
        thrownException.expectMessage("Could not read from file: " + filepath + "\nPlease check that specified market data file is correct and try again.");
        marketDataBuilder.buildMarketData(filepath);
    }

    @Test
    public void shouldNotThrowExceptionWhenProvidedFileExists() throws Exception {
        Path dataDir = Paths.get("src","test","data");
        String filepath = dataDir + "/exampleMarketData.csv";
        marketDataBuilder.buildMarketData(filepath);
    }

    @Test
    public void shouldReturnListOfLendersFromMarketData() throws Exception {
        Path dataDir = Paths.get("src","test","data");
        String expectedLenders = new String(Files.readAllBytes(Paths.get(dataDir + "/expectedLoadedMarketData")))
                .replace("\n", "");
        String filepath = dataDir + "/testLoadMarketData.csv";
        List<Lender> lenders = marketDataBuilder.buildMarketData(filepath);
        String lendersToString = "";
        for(Lender lender : lenders) {
            lendersToString = lendersToString
                    .concat(lender.toString())
                    .replace("\n", "");;
        }
        assertEquals(expectedLenders, lendersToString);
    }
}