package service.loan;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;
import service.loan.io.LoanQuoteCli;
import service.loan.io.MarketDataBuilder;
import service.loan.model.Lender;
import service.loan.model.LenderPool;
import service.loan.model.Quote;

import java.io.FileNotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class LoanQuoteServiceTest {
    private LoanQuoteService loanQuoteService;

    @Spy
    private LoanQuoteCli loanQuoteCli;

    @Mock
    private LoanCalculator loanCalculator;

    @Mock
    private MarketDataBuilder marketDataBuilder;

    @Mock
    private LenderPool lenderPool;

    @Before
    public void setUp() throws Exception {
        loanQuoteService = new LoanQuoteService(
                loanQuoteCli,
                loanCalculator,
                marketDataBuilder,
                lenderPool);
    }

    @After
    public void tearDown() throws Exception {
        Mockito.reset();
    }

    @Test
    public void shouldCatchIllegalArgumentExceptionAndPrintExceptionMessage() throws Exception {
        String[] args = {};
        doThrow(new IllegalArgumentException("some exception message")).when(loanQuoteCli).parseCliArgs(args);

        loanQuoteService.run(args);
        verify(loanQuoteCli).printLine("some exception message");
    }

    @Test
    public void shouldCatchFileNotFoundExceptionAndPrintExceptionMessage() throws Exception {
        String[] args = {"fileDoesNotExist", "1500"};
        doThrow(new FileNotFoundException("some exception message")).when(marketDataBuilder).buildMarketData("fileDoesNotExist");

        loanQuoteService.run(args);
        verify(loanQuoteCli).printLine("some exception message");
    }

    @Test
    public void shouldBuildMarketDataFromFile() throws Exception {
        String[] args = {"data/exampleMarketData.csv", "1500"};
        loanQuoteService.run(args);
        verify(marketDataBuilder).buildMarketData("data/exampleMarketData.csv");
    }

    @Test
    public void shouldPrintQuoteUnavailableMessageWhenNoQuoteIsReturned() throws Exception {
        String[] args = {"exampleMarketData.csv", "1500"};
        doReturn(new ArrayList<Lender>()).when(marketDataBuilder).buildMarketData("exampleMarketData.csv");
        doReturn(Optional.empty()).when(loanCalculator).getQuote(any(), any());

        loanQuoteService.run(args);
        verify(loanQuoteCli).printLine("We are unable to provide a quote at this time. Please try again later.");
    }

    @Test
    public void shouldPrintRequestedLoanValidationMessageWhenRequestedAmountIsInvalid() throws Exception {
        String[] args = {"exampleMarketData.csv", "850"};
        doReturn(new ArrayList<Lender>()).when(marketDataBuilder).buildMarketData("exampleMarketData.csv");

        loanQuoteService.run(args);
        verify(loanQuoteCli).printLine("Please enter a valid loan amount. Loans can be requested in £100 increments, from £1000 up to £15000 inclusive.");
    }

    @Test
    public void shouldPrintQuoteWhenQuoteIsReturned() throws Exception {
        String[] args = {"exampleMarketData.csv", "1000"};
        doReturn(new ArrayList<Lender>()).when(marketDataBuilder).buildMarketData("exampleMarketData.csv");
        Quote mockQuote = Mockito.mock(Quote.class);
        doReturn("String output for a valid quote").when(mockQuote).toString();
        doReturn(Optional.of(mockQuote)).when(loanCalculator).getQuote(any(), any());

        loanQuoteService.run(args);
        verify(loanQuoteCli).printLine("String output for a valid quote");
    }

    @Test
    public void shouldBeFalseWhenRequestedLoanAmountIsLessThan1000() throws Exception {
        boolean withinRequestLimits = loanQuoteService.isWithinRequestLimits(BigDecimal.valueOf(999));
        assertFalse(withinRequestLimits);
    }

    @Test
    public void shouldBeTrueWhenRequestedLoanAmountIsEqualTo1000() throws Exception {
        boolean withinRequestLimits = loanQuoteService.isWithinRequestLimits(BigDecimal.valueOf(1000));
        assertTrue(withinRequestLimits);
    }

    @Test
    public void shouldBeTrueWhenRequestedLoanAmountIsBetween1000And1500() throws Exception {
        boolean withinRequestLimits = loanQuoteService.isWithinRequestLimits(BigDecimal.valueOf(1250));
        assertTrue(withinRequestLimits);
    }

    @Test
    public void shouldBeTrueWhenRequestedLoanAmountIsEqualTo1500() throws Exception {
        boolean withinRequestLimits = loanQuoteService.isWithinRequestLimits(BigDecimal.valueOf(1500));
        assertTrue(withinRequestLimits);
    }

    @Test
    public void shouldBeFalseWhenRequestedLoanAmountIsGreaterThan1500() throws Exception {
        boolean withinRequestLimits = loanQuoteService.isWithinRequestLimits(BigDecimal.valueOf(1501));
        assertFalse(withinRequestLimits);
    }

    @Test
    public void shouldBeFalseWhenRequestedLoanAmountIsNotAMultipleOf100() throws Exception {
        boolean validLoanDenomination = loanQuoteService.isValidLoanDenomination(BigDecimal.valueOf(1250));
        assertFalse(validLoanDenomination);
    }

    @Test
    public void shouldBeTrueWhenRequestedLoanAmountIsAMultipleOf100() throws Exception {
        boolean validLoanDenomination = loanQuoteService.isValidLoanDenomination(BigDecimal.valueOf(1300));
        assertTrue(validLoanDenomination);
    }

    @Test
    public void shouldBeTrueWhenValidDenominationAndWithinLimits() throws Exception {
        boolean validLoanRequest = loanQuoteService.isValidLoanRequest(BigDecimal.valueOf(1200));
        assertTrue(validLoanRequest);
    }

    @Test
    public void shouldBeFalseWhenValidDenominationAndNotWithinLimits() throws Exception {
        boolean validLoanRequest = loanQuoteService.isValidLoanRequest(BigDecimal.valueOf(1600));
        assertFalse(validLoanRequest);
    }

    @Test
    public void shouldBeFalseWhenNotValidDenominationAndWithinLimits() throws Exception {
        boolean validLoanRequest = loanQuoteService.isValidLoanRequest(BigDecimal.valueOf(1350));
        assertFalse(validLoanRequest);
    }
}