package service.loan;

import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;

public class InterestCalculatorTest {

    @Test
    public void calculateMonthlyCompoundingInterestUsingBigDecimals() throws Exception {
        BigDecimal principalAmount = new BigDecimal(1000);
        BigDecimal interestRate = new BigDecimal(0.07);
        int period = 36;
        BigDecimal compoundInterest = InterestCalculator.monthlyCompoundingInterest(principalAmount, interestRate, period);
        assertEquals(BigDecimal.valueOf(30.87), compoundInterest);
    }

    @Test
    public void calculateMonthlyCompoundingInterestForZeroInterestRateUsingBigDecimals() throws Exception {
        BigDecimal principalAmount = new BigDecimal(1000);
        BigDecimal interestRate = BigDecimal.ZERO;
        int period = 36;
        BigDecimal compoundInterest = InterestCalculator.monthlyCompoundingInterest(principalAmount, interestRate, period);
        assertEquals(BigDecimal.valueOf(27.77), compoundInterest);
    }

    @Test
    public void calculateMonthlyCompoundingInterestUsingDoubles() throws Exception {
        double compoundInterest = InterestCalculator.monthlyCompoundingInterest(1000, 0.07, 36);
        assertEquals(30.87, compoundInterest, 0.1);
    }

    @Test
    public void calculateMonthlyCompoundingInterestForZeroInterestRateUsingDoubles() throws Exception {
        double compoundInterest = InterestCalculator.monthlyCompoundingInterest(1000, 0.0, 36);
        assertEquals(27.77, compoundInterest, 0.1);
    }
}