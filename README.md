# loan-quote ![circle-ci-status](https://circleci.com/bb/emsiow/loan-quote.png?circle-token=0291b7a4cc8ceaaec6baf09206337729fc052ae0)

Loan quoting service for peer-to-peer lending

## Design notes
Loan 'chunks' of £10 are allocated round-robin style from a list of lenders (sorted in ascending order by their offered interest rate).
This is done until the requested loan amount can be met or until the lending pool runs out of available loan chunks.

Assumptions:

- loan chunks are set at £10

- repayment period is set at 36 months

- loan chunks cannot be comprised of composite sub-chunks,
    
    - ie. you can't allocate £10 based on Lender A having £9 remaining and Lender B having £1 remaining to loan.    

## Java

http://www.oracle.com/technetwork/java/javase/downloads/index.html

Project uses Java 8

## Installing Gradle build tool

https://gradle.org/install/

eg. using Homebrew

```
> brew install gradle
```

## Installing dependencies
```
> ./gradlew clean install

```

## Runnings tests

```
> ./gradlew test
```

## Building application

```
> ./gradlew clean build
```

## Running distribution JAR

```
> java -jar path/to/distribution.jar [market_data] [loan_amount]

```

eg. if JAR and market.csv files are located in current working directory:
```
> java -jar loan-quote-1.0.0.jar market.csv 1000

```

eg. if running from project root directory after running ```./gradlew build``` command:
```
> java -jar build/libs/loan-quote-1.0.0.jar market.csv 1000

```

## Build monitor

GitHub: https://github.com/build-canaries/nevergreen

Copy ```nevergreen-config.json``` and import into [nevergreen.io](https://nevergreen.io/backup) backup configuration.

## CI Pipeline

[Circle CI](https://circleci.com/bb/emsiow/loan-quote)

Tech note: currently just runs test suite

## Story wall

[Trello](https://trello.com/b/II3nsIeW/loan-quoting)
